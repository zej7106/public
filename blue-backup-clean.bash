#!/bin/bash
work_dir="$HOME/backup"
############ email variable ############
# need sendmail 
#
sender=blue.admin@domain.com
recipient="emai@domain.com" 
subject=0 # 0=OK, 1=Error, 2=Warrning
mailbodyfile=$work_dir/mail-$(date +%Y%m%d)
mailfile=$work_dir/mail.txt

############## Remote system variable #############
ip_address="XXX.XXX.XXX.XXX"
remote_volume="<SMB_VOLUME>"
remote_username="<SMB_USERNAME>"
remote_password="<SMB_PASSWORD>"
remote_file_system="cifs"
remote_folder0="<REMOTE FOLDER NAME ON NAS"
mounted_folder="LOCAL FOLDER"
todaydir="$mounted_folder/$remote_folder0/$(date +"%Y%m%d")"


############ local backup variable ###################################
backup_logs_dir=$work_dir
start_time=$(date +%s)
logfile="$work_dir/$(hostname)_backup-$(date +%Y%m%d).log"
dirnames=(/etc /root /var/lib/samba /home/samba/shares/* /home/[!samba]*)

################# mysql variable ############
mysql_backup=1 # 1= backup mysql database
mysql_backup_user=<MY_SQL_USER>
mysql_backup_password=<MY_SQL_PASSWORD>
mysql_databases=(zentyal)

#################### Functions ####################

#--------------- Send to email ---------------#
mailsend () {
case "$subject" in
0)	echo "Backup is OK"
	subjecttext="Backup is full"
;;
1)	echo "Backup FAILD"
	subjecttext="Backup FAILD"
;;
2)	echo "Backup is OK with warnning"
	subjecttext="Backup finished with error"
;;
esac

echo "From: $sender" >>$mailfile
echo "To: $recipient" >>$mailfile
echo "Subject: $subjecttext" >>$mailfile
echo "">>$mailfile
cat $mailbodyfile >>$mailfile 
cat $mailfile | /usr/sbin/sendmail -t

exit
}
#--------------- Mount Volume ---------------# 
mount_volume () {
	ping -c 2 $ip_address >>/dev/null
	code=$?
	if [ "$code" = "0" ]; then {
                subject=0
		mountpoint $mounted_folder >>/dev/null
		mpkod=$?
		if [ "$mpkod" = "1" ]; then {
			mount -t $remote_file_system //$ip_address/$remote_volume $mounted_folder -o username=$remote_username,password=$remote_password
			mkod=$?
				if [ "$mkod" = "0" ]; then {
					echo "$(date +%Y.%m.%d-%k:%M.%S) //$ip_address/$remote_volume mounted" >>$logfile
					subject=0
				}
				else {
					echo "$(date +%Y.%m.%d-%k:%M.%S) can't mounted //$ip_address/$remote_volume " >>$logfile
					subject=1
					echo "$(date +%Y.%m.%d-%k:%M.%S) FATAL ERROR! I can't mounted //$ip_address/$remote_volume" >>$mailbodyfile
					mailsend
				}
				fi
		}
		else {
                echo "$(date +%Y.%m.%d-%k:%M.%S)  //$ip_address/$remote_volume's already mounted " >>$logfile
                subject=0
		}
		fi
	}
	else {
		echo "$(date +%Y.%m.%d-%k:%M.%S) can't ping to $ip_address " >>$logfile
                subject=1
                echo "$(date +%Y.%m.%d-%k:%M.%S) FATAL ERROR! I can't ping $ip_address" >>$mailbodyfile
                mailsend
	
	}
	fi
}
#--------------------Backup mysql databases------------------------#
mysql_backup (){
		echo "Database Backup" >>$mailbodyfile
		echo "Start: $(date +%Y.%m.%d-%k:%M.%S)" >>$mailbodyfile

for i in ${mysql_databases[*]}
 do
		echo "$i database backup started"
		echo "$(date +%Y.%m.%d-%k:%M.%S) Database: "$i" DumpFileName: "$i"-dump.sql" >>$logfile
		mysqldump -u$mysql_backup_user -p$mysql_backup_password $i >$todaydir/$i"-dump.sql"
		mysqlcode=$?
		if [ "$mysqlcode" = "0" ]; then {
			echo "$i Database saved" >>$mailbodyfile
			echo "$(date +%Y.%m.%d-%k:%M.%S) $i Database saved" >>$logfile
		}
		else {
			subject=2
			echo "$i database dump error " >>$mailbodyfile
                        echo "$(date +%Y.%m.%d-%k:%M.%S) $i database dump error" >>$logfile
		}
		fi
		 echo "$i database backup finished"

done
                echo "Finished: $(date +%Y.%m.%d-%k:%M.%S)" >>$mailbodyfile
                echo "*****************************************************************************************************************************" >>$mailbodyfile
}
#------------------------ Backup files ----------------------------#
files_backup (){
		echo "Backup of files" >>$mailbodyfile
                echo "Start: $(date +%Y.%m.%d-%k:%M.%S)" >>$mailbodyfile

for dirs in ${dirnames[*]}; do
                        tempdirname=$(echo $dirs | rev |cut -f1 -d'/')
                        subdirname=$(echo $dirs | cut -c 2-)
			filename=$(echo $tempdirname | rev)-$(date +"%Y-%m-%d").tar.gz
                        mkdir -p $todaydir/$subdirname
		 	echo "tar -czf $todaydir/$subdirname/$filename $dirs"
			tar -czf $todaydir/$subdirname/$filename $dirs
			echo "tar -czf $todaydir/$subdirname/$filename" >>/root/backup/param.txt
			tar_code=$?
			if [ "$tar_code" = "0" ];then {
				tar -tzf $todaydir/$subdirname/$filename >>$todaydir/$subdirname/$filename.txt
					untar_code=$?
					if [ "$untar_code" = "0" ]; then {
					 echo " $todaydir/$subdirname/$filename is OK" >>$logfile
					 echo " $todaydir/$subdirname/$filename is OK! Return code: $untar_code" >>$mailbodyfile
					}
					else {
					echo " $todaydir/$subdirname/$filename is wrong Return code: $untar_code" >>$logfile
                                        echo " $todaydir/$subdirname/$filename is wrong Return code: $untar_code" >>$mailbodyfile
					subject=2
					}
					fi

			}
			else {
				echo " Warrning! Backup of $todaydir/$subdirname/$filename is failed. Return code: $tar_code" >>$logfile
				echo " Warrning! Backup of $todaydir/$subdirname/$filename is failed. Return code: $tar_code" >>$mailbodyfile
				subject=2
			}
			fi 
                done


		echo "Finished: $(date +%Y.%m.%d-%k:%M.%S)" >>$mailbodyfile
                echo "*****************************************************************************************************************************" >>$mailbodyfile

}

#----------------------Create info file----------------------------#
create_info (){
 		echo -e "******************************** OS release ********************************" >$todaydir/info-$(date +"%Y-%m-%d").txt
                cat /etc/os-release >>$todaydir/info-$(date +"%Y-%m-%d").txt

                echo -e "\n******************************* Network info *******************************">>$todaydir/info-$(date +"%Y-%m-%d").txt
                netstat -ie >>$todaydir/info-$(date +"%Y-%m-%d").txt

                echo -e "\n******************************* Memory info *******************************">>$todaydir/info-$(date +"%Y-%m-%d").txt
                free -m >>$todaydir/info-$(date +"%Y-%m-%d").txt


                echo -e "\n***************************** free disk space *****************************" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                df -h | grep "Filesystem" >>$todaydir/info-$(date +"%Y-%m-%d").txt 
                df -h | grep -v "/media/samba" | grep "/dev/">>$todaydir/info-$(date +"%Y-%m-%d").txt 

		echo -e "\n***************************** free disk space *****************************" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                df -h | grep "Filesystem" >>$mailbodyfile 
                df -h | grep -v "/media/samba" | grep "/dev/">>$mailbodyfile 


                echo -e "\n******************************** Last log *********************************" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                lastlog  | grep -v "**Never logged in**">>$todaydir/info-$(date +"%Y-%m-%d").txt
                echo -e "\n******************************** fotgroup.com domain  *********************************" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                        net getdomainsid >>$todaydir/info-$(date +"%Y-%m-%d").txt
                        echo -e "\n" >>$todaydir/info-$(date +"%Y-%m-%d").txt 
                        net status sessions >>$todaydir/info-$(date +"%Y-%m-%d").txt 
                        echo -e "\n" >>$todaydir/info-$(date +"%Y-%m-%d").txt 
                        net ads info >>$todaydir/info-$(date +"%Y-%m-%d").txt 
                        echo -e "\n Samba Version: " >>$todaydir/info-$(date +"%Y-%m-%d").txt 
                        smbstatus -V >>$todaydir/info-$(date +"%Y-%m-%d").txt



}
#------------------------------------------------------------------#

rm -f $mailbodyfile >>/dev/null
rm -f $mailfile >>/dev/null
if [ ! -d $work_dir ]; then {
		mkdir $work_dir
}
fi

echo "*****************************************************************************************************************************" >>$mailbodyfile
echo "Backup started: $(date +%Y.%m.%d-%k:%M.%S)" >>$mailbodyfile
echo "*****************************************************************************************************************************" >>$mailbodyfile

mount_volume
mkdir -p $todaydir
if [ ! -d $todaydir ]; then {
	echo "$(date +%Y.%m.%d-%k:%M.%S) I couldn't create $todaydir" >>$mailbodyfile
	echo "$(date +%Y.%m.%d-%k:%M.%S) I couldn't create $todaydir" >>$logfile
	mailsend
}
fi
echo "$todaydir l�tezik"
if [ "$mysql_backup" = "1" ]; then {
		echo "Start mysql backup"
		mysql_backup

	}
fi
files_backup
create_info 
cp $todaydir/info-$(date +"%Y-%m-%d").txt $work_dir
umount $mounted_folder
echo "*****************************************************************************************************************************" >>$mailbodyfile
echo "Backup finished: $(date +%Y.%m.%d-%k:%M.%S)" >>$mailbodyfile
end_time="$(date +%s)"
longtime=$(((end_time - start_time)/60))
echo "lenght of time: $longtime minutes" >>$mailbodyfile
mailsend
