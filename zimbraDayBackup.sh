#!/bin/bash
ZHOME=/opt/zimbra
ZBACKUP=/backup/mailbox
ZCONFD=$ZHOME/conf
YEAR=$(date +%Y)
MONTH=$(date +%b)
DATE=$(date +%d)
TIME=$(date +%k)
ZDUMPDIR=$ZBACKUP/$YEAR/$MONTH/$DATE/$TIME
ZMBOX=/opt/zimbra/bin/zmmailbox
DAY=$(date -d -"1 day" +%m/%d/%Y)
echo $ZDUMPDIR
if [ ! -d $ZDUMPDIR ]; then
mkdir -p $ZDUMPDIR
fi

echo " $(date +%Y.%m.%d-%k:%M.%S) Running zmprov ... " >$ZDUMPDIR/zimbrabackup.log

       for mbox in `/opt/zimbra/bin/zmprov -l gaa`
do
	echo " $(date +%Y.%m.%d-%k:%M.%S) Generating files from backup $mbox ... " >>$ZDUMPDIR/zimbrabackup.log
	$ZMBOX -z -m $mbox getRestURL "//?fmt=tgz&query=after:$DAY" > $ZDUMPDIR/$mbox.tgz 2>>$ZDUMPDIR/zimbrabackup.log
done
echo "$(date +%Y.%m.%d-%k:%M.%S) Generating files from backup meetingrooms and projector" >>$ZDUMPDIR/zimbrabackup.log
	/opt/zimbra/bin/zmmailbox -z -m meetingroom1@domainname.hu getRestURL "//?fmt=tgz&query=after:$DAY" >$ZDUMPDIR/meetingroom1@domainname.hu.tgz  2>>$ZDUMPDIR/zimbrabackup.log
	/opt/zimbra/bin/zmmailbox -z -m meetingroom2@domainname.hu getRestURL "//?fmt=tgz&query=after:$DAY" >$ZDUMPDIR/meetingroom2@domainname.hu.tgz 2>>$ZDUMPDIR/zimbrabackup.log
	/opt/zimbra/bin/zmmailbox -z -m projector@domainname.hu getRestURL "//?fmt=tgz&query=after:$DAY" > $ZDUMPDIR/projector@domainname.hu.inf.tgz 2>>$ZDUMPDIR/zimbrabackup.log

echo "$(date +%Y.%m.%d-%k:%M.%S)  Backup END " >>$ZDUMPDIR/zimbrabackup.log
mail -s "Zimbra backup - Day backup -" -c admin@domainname.hu <$ZDUMPDIR/zimbrabackup.log

