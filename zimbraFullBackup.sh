#!/bin/bash
ZHOME=/opt/zimbra
ZBACKUP=/backup/mailbox
ZCONFD=$ZHOME/conf
YEAR=$(date +%Y)
MONTH=$(date +%b)
DAY=$(date +%d)
ZDUMPDIR=$ZBACKUP/$YEAR/$MONTH/$DAY/FULL
ZARCDIR=/backup/dump/$YEAR/$MONTH/$DAY
ZMBOX=/opt/zimbra/bin/zmmailbox
if [ ! -d $ZDUMPDIR ]; then
mkdir -p $ZDUMPDIR
fi
if [ ! -d $ZARCDIR ]; then
mkdir -p $ZARCDIR
fi
echo " $(date +%Y.%m.%d-%k:%M.%S) Running zmprov ... " >$ZDUMPDIR/zimbrabackup.log

       for mbox in `/opt/zimbra/bin/zmprov -l gaa`
do
echo " $(date +%Y.%m.%d-%k:%M.%S) Generating files from backup $mbox ... " >>$ZDUMPDIR/zimbrabackup.log

       $ZMBOX -z -m $mbox getRestURL "//?fmt=tgz" > $ZDUMPDIR/$mbox.tgz 2>>$ZDUMPDIR/zimbrabackup.log
done
echo "$(date +%Y.%m.%d-%k:%M.%S) Generating files from backup meetingrooms and projector" >>$ZDUMPDIR/zimbrabackup.log
	/opt/zimbra/bin/zmmailbox -z -m meetingroom1@domainname.hu getRestURL "//?fmt=tgz" > $ZDUMPDIR/meetingroom1@domainname.hu.tgz 2>>$ZDUMPDIR/zimbrabackup.log
	/opt/zimbra/bin/zmmailbox -z -m meetingroom2@domainname getRestURL "//?fmt=tgz" > $ZDUMPDIR/meetingroom2@domainname.hu.tgz 2>>$ZDUMPDIR/zimbrabackup.log
	/opt/zimbra/bin/zmmailbox -z -m projector@domainname getRestURL "//?fmt=tgz" > $ZDUMPDIR/projector@domainname.hu.tgz 2>>$ZDUMPDIR/zimbrabackup.log

echo "$(date +%Y.%m.%d-%k:%M.%S) Backup /opt/zimbra " >>$ZDUMPDIR/zimbrabackup.log
su - zimbra -c "/opt/zimbra/bin/zmcontrol -v" >>$ZARCDIR/zimbraversion!!.txt
TARNAME=$(date +%Y.%m.%d)_zimbra_backup.tar
tar -czf $ZARCDIR/$TARNAME /opt/zimbra/ 2>>$ZDUMPDIR/zimbrabackup.log
echo "$(date +%Y.%m.%d-%k:%M.%S)  Backup END " >>$ZDUMPDIR/zimbrabackup.log
