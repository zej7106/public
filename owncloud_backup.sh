#!/bin/bash
logfile=/root/owncloud_backup-$(date +%Y%m%d).log
todaydir=/media/samba/owncloud.fotgroup.com/$(date +"%Y%m%d")
ping -c 2 <storage ip> >>/dev/null
kod=$?
if [ "$kod" = "0" ]; then {
                echo "$(date +%Y.%m.%d-%k:%M.%S) <storage ip> is live " >>$logfile
                mount -t cifs //<storage ip>/Volume_1 /media/samba -o username=<user_name>,password=<password>
		if [ -d /media/samba/<share_name> ]; then {
			 date >/media/samba/<share_name>/lastrunning.txt
			}
		else	{
			 mkdir /media/samba/<share_name>
			 date >/media/samba/share_name>/lastrunning.txt
			}
		fi
                if [ -d /media/samba/<share_name> ]; then {
                        mkdir -p $todaydir/owncloud
			mkdir -p $todaydir/wordpress
			mkdir -p $todaydir/speedtest
			mkdir -p $todaydir/password
                        }
                fi
		mysqldump -u<mysql_username> -p<password> owncloud >$todaydir/owncloud/owncloud_dump.sql
	        mysqldump -u<mysql_username> -p<password> wordpress >$todaydir/wordpress/wordpress_dump.sql
		for fileserver in /srv/owncloud/data/*; do
                        dirname=$(echo $fileserver | cut -f5 -d'/')
			tar -czf $todaydir/owncloud/$dirname-$(date +"%Y-%m-%d").tar.gz $fileserver
                        echo $(date +%Y.%m.%d-%k:%M.%S) $fileserver  backup finished  >>$logfile
                done
		tar -czf $todaydir/wordpress/wordpress-$(date +"%Y-%m-%d").tar.gz /var/www/wordpress
		tar -czf $todaydir/speedtest/speedtest-$(date +"%Y-%m-%d").tar.gz /var/www/speedtest
                tar -czf $todaydir/password/password-$(date +"%Y-%m-%d").tar.gz /var/www/password
  		echo -e "******************************** OS release ********************************" >$todaydir/info-$(date +"%Y-%m-%d").txt
                cat /etc/os-release >>$todaydir/info-$(date +"%Y-%m-%d").txt

                echo -e "\n******************************* Network info *******************************">>$todaydir/info-$(date +"%Y-%m-%d").txt
                netstat -ie >>$todaydir/info-$(date +"%Y-%m-%d").txt

                echo -e "\n******************************* Memory info *******************************">>$todaydir/info-$(date +"%Y-%m-%d").txt
                free -m >>$todaydir/info-$(date +"%Y-%m-%d").txt


                echo -e "\n***************************** free disk space *****************************" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                df -h | grep "Filesystem" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                df -h | grep -v "/media/samba" | grep "/dev/vd">>$todaydir/info-$(date +"%Y-%m-%d").txt

                echo -e "\n******************************** Last log *********************************" >>$todaydir/info-$(date +"%Y-%m-%d").txt
                lastlog  | grep -v "**Never logged in**">>$todaydir/info-$(date +"%Y-%m-%d").txt
}
                else {
                        echo "$(date +%Y.%m.%d-%k:%M.%S) 192.168.10.188 doesn't live" >>$logfile
                        return
}
fi

umount /media/samba
rm /srv/owncloud/data/<owncloud_user_name>/files/OC_DATA/*
mysql owncloud --delimiter=';' -u<mysql_username> -p<password> -e 'SELECT 'file_target' , 'share_with' , 'uid_owner' , 'expiration' FROM oc_share WHERE 1 ORDER BY file_target ASC LIMIT 0 , 300;' | sed 's/\t/";"/g;s/^/"/;s/$/"/;' >/srv/owncloud/data/<owncloud_user_name>/files/OC_DATA/users_shared_folder-$(date +"%Y-%m-%d").csv
du -sh /srv/owncloud/data/* | cut -s -f1,5 -d'/' --output-delimiter=';' >/srv/owncloud/data/<owncloud_user_name>/files/OC_DATA/disk_usage-$(date +"%Y-%m-%d").csv
chown -R www-data:www-data /srv/owncloud/data/<owncloud_user_name>/files/OC_DATA/
