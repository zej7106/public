@echo off

set felhasznalonev=Administrator
set mentesmappa=G:\backupscript
set naplofile=napimentes.txt

set user=%UserProfile%
set user=%user:"=%
set folder=%user%\Local Settings\Application Data\Microsoft\Windows NT\NTBackup\data

for /f "Tokens=1-4 Delims=/ " %%i in ('date /t') do set dt=%%i-%%j-%%k-%%l
for /f "Tokens=1-4 Delims=/ " %%i in ('date /t') do set datum=%%l_%%j_%%k_%%i
for /f "Tokens=1" %%i in ('time /t') do set tm=-%%i
for /f "Tokens=1" %%i in ('date /t') do set day=%%i

set keszlet="HP Ultrium 1-SCSI SCSI Sequential Device"
set tm=%tm::=-%
set dtt=%dt%%tm%
set feladat=%day%-backup_notes
set napiment=morganabackup_%datum%.bkf
set tapename=2009_%day%
realdate /f="CCYY.MM.DD hh:mm:ss -- Napi ment�s elindult." /e >>%mentesmappa%\%naplofile%
del "C:\Documents and Settings\%felhasznalonev%\Local Settings\Application Data\Microsoft\Windows NT\NTbackup\Data\*.log"

rem az id�zojelek k�z�tti nevet a "Cser�lheto t�rol� kezel�s" MMC-bol lehet megn�zni
rsm.exe refresh /LF"HP Ultrium 1-SCSI SCSI Sequential Device"
echo refresh v�grehajtva
wait 30

rem a *.bks file az ntbackup.exe seg�ts�g�vel �ll�that� elo �s k�sobb azzal m�dos�that�



rem --- 14 napn�l r�gebbi f�jlok elt�vol�t�sa --
robocopy h:\backup\ h:\purge /minage:14 /move
del /q h:\purge\*.*
rem ---		


rem **file-ba ment�s **
	C:\WINDOWS\system32\ntbackup.exe backup "@C:\Documents and Settings\Administrator\Local Settings\Application Data\Microsoft\Windows NT\NTBackup\data\notesfull.bks" /a /d "Set created %dt%" /v:no /r:no /rs:no /hc:off /m normal /j "backup file %feladat%" /l:f /f "H:\Backup\%napiment%"

rem ** Szalagra ment�s **
	C:\WINDOWS\system32\ntbackup.exe backup "@C:\Documents and Settings\Administrator\Local Settings\Application Data\Microsoft\Windows NT\NTBackup\data\notesfull.bks" /t "%tapename%" /n "%tapename%" /d "Set created %dt%" /v:no /r:no /rs:no /hc:on /m normal /j "backup DAT %feladat%" /l:f
	If %errorlevel% == 0 (rsm.exe eject /LF"HP Ultrium 1-SCSI SCSI Sequential Device")

	rem rsm.exe eject /PF"monday" /astart
	realdate /f="CCYY.MM.DD hh:mm:ss -- Napi ment�s lefutott." /e >>%mentesmappa%\%naplofile%

If %day% == Fri (goto fri) else (goto end)

rem P�nteki ment�s k�l�n v�laszt�sa
:fri
move /y "H:\Backup\%napiment%" G:\backup

:end

del *.log
copy "%folder%\*.log" g:\backupscript
del "%folder%\*.log"
del morganabackup.log
del naplo.txt
dir h:\backup >>naplo.txt
dir g:\backup >>naplo.txt

echo *=- C:\ -= System* >>naplo.txt
fsutil volume diskfree c: >>naplo.txt

echo *=- E:\ -=* Notes Data >>naplo.txt
fsutil volume diskfree E: >>naplo.txt

echo *=- G:\ -=* Backup Friday >>naplo.txt
fsutil volume diskfree g: >>naplo.txt

echo *=- H:\ -=* Backup >>naplo.txt
fsutil volume diskfree H: >>naplo.txt


copy /A *.log morganabackup.log 
